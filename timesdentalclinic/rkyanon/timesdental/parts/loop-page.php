<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
	<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
		
		<?php if ( !is_home() && !is_front_page() && $post->post_name != "faq") : ?>	

			<header class="article-header">

				<h3 class="page-title"><?php the_title(); ?></h3>

			</header> <!-- end article header -->

		<?php endif; ?>		

	    <section class="entry-content" itemprop="articleBody">

		    <?php the_content(); ?>

		    <?php wp_link_pages(); ?>

		</section> <!-- end article section -->
							
		<footer class="article-footer">
			
		</footer> <!-- end article footer -->
							    
		<?php comments_template(); ?>
						
	</article> <!-- end article -->
	
<?php endwhile; endif; ?>							