<?php if ( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Page extends MY_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->helper('form');
		$this->landingpage();
	}

	public function landingpage() {
		$this->_set_title('Times Dental Clinic');

		$this->_set_section('header', 'section/header', $this->data);
		$this->_set_section('offcanvas', 'section/offcanvas', $this->data);
		$this->_set_section('footer', 'section/footer', $this->data);
		$this->_set_layout('default');

        $this->_render_page('home', $this->data);
	}

	public function about() {
		$this->_set_title('Times Dental Clinic: About Us');
		$this->_set_section('header', 'section/header', $this->data);
		$this->_set_section('offcanvas', 'section/offcanvas', $this->data);
		$this->_set_section('footer', 'section/footer', $this->data);
		$this->_set_layout('default');

        $this->_render_page('about_us', $this->data);
	}

	public function faq() {
		$this->_set_title('Times Dental Clinic: Frequently Asked Questions');
		$this->_set_section('header', 'section/header', $this->data);
		$this->_set_section('offcanvas', 'section/offcanvas', $this->data);
		$this->_set_section('footer', 'section/footer', $this->data);
		$this->_set_layout('default');

        $this->_render_page('faq', $this->data);
	}

	public function contact_us() {
		$this->_set_title('Times Dental Clinic: Contact Us');
		$this->_set_section('header', 'section/header', $this->data);
		$this->_set_section('offcanvas', 'section/offcanvas', $this->data);
		$this->_set_section('footer', 'section/footer', $this->data);
		$this->_set_layout('default');

        $this->_render_page('contact-us', $this->data);
	}

	public function signature() {
		$this->_set_title('Times Dental Clinic: Contact Us');
		$this->_set_layout('simple');
        $this->_render_page('signature', $this->data);
	}


	public function quickmsg() {
		$this->_set_title('Times Dental Clinic: Frequently Asked Questions');
		$this->_set_section('header', 'section/header', $this->data);
		$this->_set_section('offcanvas', 'section/offcanvas', $this->data);
		$this->_set_section('footer', 'section/footer', $this->data);
		$this->_set_layout('default');

		$this->load->library('form_validation');
		$this->form_validation->set_rules('client_name', 'Name', 'trim|required');
		$this->form_validation->set_rules('client_email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('client_phone', 'Phone', 'required');
		$this->form_validation->set_rules('client_message', 'Message', 'required');

		if ($this->form_validation->run() == FALSE) {
			$this->form_validation->set_error_delimiters('<small class="error">', '</small>');
			$this->index();
		} else {
			$this->load->view('formsuccess');
		}

		//$this->_render_page('quick-msg', $this->data);
	}
}

/* End of file example.php */
/* Location: ./application/controllers/example.php */