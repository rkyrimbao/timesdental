<div class="row">
	<div class="large-4 columns">
		aside
	</div>
	<div class="large-8 columns">
		<h5><strong>ABOUT US</strong></h5>
		<p>Our expertise focuses on the complete transformation of your smile. Like you, your smile is unique. We take great care in designing a smile that suits you perfectly.</p>
		<p>Helmed by one of the Philippines’ devoted dentists, <strong>Dr. Jayhardt Tantiado</strong>, TIMES DENTAL CLINIC is comprised of highly competent, and committed variety of doctors in the medicalfield. We have the finest team of dental specialists, dental hygienists and support staff specialized in General Dentistry, Orthodontics, Oral Surgery, and Pedodontics. These individuals have earned considerable years of experience creating distinctive and bespoke ways to provide quality service.</p>

		<h5><strong>MISSION</strong></h5>
		<p>Our MISSION is to provide distinguished services that go beyond our patients’ satisfaction through personalized, honest, ethical and informed dental practices, procedures and treatments carried out in a comfortable, warm and friendly environment.</p>
		<p>Moreover, we aim to help our community achieve excellent oral healthcare. In this light, we ensure to give high quality, multidisciplinary and cost efficient dental assistance.</p>

		<h5><strong>VISION</strong></h5>
		<p>Our VISION is to become recognized as the leading dental health provider in the city. Our vision is synonymous to the trust and care that we aim to give to our clients.</p>
		
	</div>
</div>