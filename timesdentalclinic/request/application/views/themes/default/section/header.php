<?php if (!defined('BASEPATH')) exit('No direct script access allowed...'); ?>

<section class="header-section show-for-large-up">
  <div class="header-bar">
    <div class="row">
      <div class="large-12 columns">
        <nav class="top-bar" data-topbar role="navigation">
          <ul class="title-area">
            <li class="name">
              <h1 class="site-title"><?php echo anchor(base_url(), '<img src="'.$assets_dir.'/img/blank.gif" class="site-brand">', array('class' => 'site-logo')); ?></h1>
            </li>
          </ul>

          <section class="top-bar-section top-bar-nav">
            <ul class="right">     
              <li<?php if ($this->uri->uri_string() == '') : ?> class="active" <?php endif; ?> ><?php echo anchor(base_url(), 'Home'); ?></li>
              <li<?php if ($this->uri->uri_string() == 'page/about') : ?> class="active" <?php endif; ?>><?php echo anchor('about', 'About Us'); ?></li>
              <li<?php if ($this->uri->uri_string() == 'page/services') : ?> class="active" <?php endif; ?>><a href="">Our Services</a></li>
              <li<?php if ($this->uri->uri_string() == 'appointment') : ?> class="active" <?php endif; ?>><?php echo anchor(base_url().'appointment', 'Appointment Request'); ?></li>
              <li<?php if ($this->uri->uri_string() == 'page/faq') : ?> class="active" <?php endif; ?>><?php echo anchor('faq', 'FAQ'); ?></li>
              <li<?php if ($this->uri->uri_string() == 'page/contact_us') : ?> class="active" <?php endif; ?>><?php echo anchor('contact_us', 'Contact Us'); ?></li>
            </ul>
          </section>

        </nav>
      </div>
    </div>
  </div>
</section>
 